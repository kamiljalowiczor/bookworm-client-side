import React from 'react';
import classes from './Footer.module.css';
import Aux from '../../../hoc/Auxiliary'

import Image from '../../UI/Image/Image'
import logo from '../../../assets/bookworm-logo2.png'

const footer = (props) => {
    return (
        <Aux>
            <div className={classes.Space}></div>
            <footer className={classes.Footer}>
                <div className={classes.Container}>
                    <div className={classes.FooterContainer}> 
                        <div>
                            <Image src={logo} alt="logo" size="large" noshadow />
                        </div>                        
                        <div>          
                            <p>Created by students of group 32A</p>   
                            <p>West Pomeranian University of Technology, Szczecin</p> 
                            <p>2019-2020</p>
                        </div>
                    </div>
                </div>
            </footer>
        </Aux>
        
    )
}

export default footer;