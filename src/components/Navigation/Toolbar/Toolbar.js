import React from 'react';
import SearchBar from '../../UI/SearchBar/SearchBar'
import NavigationItems from '../NavigationItems/NavigationItems';
import HamburgerMenu from '../HamburgerMenu/HamburgerMenu'
import Image from '../../UI/Image/Image';
import classes from './Toolbar.module.css';

import image from '../../../assets/bookworm-logo2.png';

const toolbar = props => {
    return (
        <header className={classes.Toolbar}>
            <HamburgerMenu clicked={props.hamburgerClickedHandler} />
            <div className={classes.Logo}>
                <Image src={image} alt="logo" noshadow />
            </div>
            <div className={classes.DesktopOnly}>
                <SearchBar />
                <nav>
                    <NavigationItems isAuthenticated={props.isAuth} />
                </nav>
            </div>

        </header>
    );
}

export default toolbar;