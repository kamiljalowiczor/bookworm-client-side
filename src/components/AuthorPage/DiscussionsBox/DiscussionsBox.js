import React from 'react';

import Section from '../../UI/Section/Section';
import List from '../../UI/List/List'
import DiscussionBoxItem from './DiscussionBoxItem/DiscussionBoxItem';
import Input from '../../UI/Input/Input'
import Button from '../../UI/Button/Button'

import classes from './DiscussionsBox.module.css';


const discussionsBox = (props) => {
    return (
        <Section title="Ongoing discussions ||MISSING||">
            <Input elementType="input" elementConfig= {{type: "text", placeholder:"Search for a discussion"}} />
            <List>
                <li className={classes.ListItem}>
                    <DiscussionBoxItem title="Pan Tadeusz is his only good book" userName="user123" />
                </li>
                <li className={classes.ListItem}>
                    <DiscussionBoxItem title="Pan Tadeusz is his only good book" userName="user123" />
                </li>
                <li className={classes.ListItem}>
                    <DiscussionBoxItem title="Pan Tadeusz is his only good book" userName="user123" />
                </li>
                <li className={classes.ListItem}>
                    <DiscussionBoxItem title="Pan Tadeusz is his only good book" userName="user123" />
                </li>
                <li className={classes.ListItem}>
                    <DiscussionBoxItem title="Pan Tadeusz is his only good book" userName="user123" />
                </li>
                <li className={classes.ListItem}>
                    <DiscussionBoxItem title="Pan Tadeusz is his only good book" userName="user123" />
                </li>
            </List>
            <Button style={{width: "100%", margin: "0"}}>Start a new discussion!</Button>
        </Section>
    );
}

export default discussionsBox;