import React from 'react';

import Section from '../../UI/Section/Section';
import Review from '../../BookPage/Reviews/Review/Review'
import User from '../../UI/User/User'
// import StarRating from '../../UI/StarRating/StarRating';
// import Aux from '../../../hoc/Auxiliary';

import classes from './LatestReview.module.css'

const latestReview = (props) => {
    const heading = (
        <div className={classes.Heading}>
            Latest review for this author's book
            <div className={classes.HeadingBottom}>
            </div>
        </div>
    )
    return (
        <Section title={heading} >
            <Review noHeader noFooter noRating review={props.review}/>
        </Section>
    )

}

export default latestReview;