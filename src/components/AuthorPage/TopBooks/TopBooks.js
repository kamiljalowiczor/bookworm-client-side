import React from 'react';

import Section from '../../UI/Section/Section'
import TopBook from './TopBook/TopBook'
import List from '../../UI/List/List'

//import classes from './TopBooks.module.css';

const topBooks = (props) => {
    const sizeArray = ["big", "medium", "small"];

    let topBooks = props.books.map((book, index) => {
        return <li key={book.id}><TopBook book={book} size={sizeArray[index]}/></li>
    });

    let title = "Top books"
    if (topBooks.length > 3) {
        topBooks = topBooks.slice(0, 3);
        title = "Top 3 books";
    }

    return (
        <Section title={title}>
            <List>
                {topBooks}
            </List>
        </Section>
    );
}

export default topBooks;