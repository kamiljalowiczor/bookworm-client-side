import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import classes from './BookInfo.module.css';

import Image from '../../../UI/Image/Image'
import Heading from '../../../UI/Text/Heading/Heading'

class BookInfo extends Component {
    redirect = () => {
        this.props.history.push(`/author/${this.props.author}`);
    }

    render() {
        return (
            <div className={classes.InfoContainer}>
                <Image src={this.props.image} alt="book" size="big" style={{marginRight: "2rem"}}/>
                <div className={classes.BookDescription}>
                    <h3 size="small" className={classes.AuthorName} onClick={() => this.redirect()}>{this.props.author}</h3>
                    <Heading size="big" style={{color: "rgb(255, 192, 17)", marginBottom: "0.2rem"}}>{this.props.bookName}</Heading>
                    <ul>
                        <li>Released: {this.props.released}</li>
                        <li>Category: {this.props.genre}</li>
                        <li>ISBN: {this.props.isbn}</li>
                    </ul>  
                </div>
            </div>
        );
    }
}

export default withRouter(BookInfo);