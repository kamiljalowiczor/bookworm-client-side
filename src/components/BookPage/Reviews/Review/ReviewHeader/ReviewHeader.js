import React from 'react';

import User from '../../../../UI/User/User'
// import StarRating from '../../../../UI/StarRating/StarRating';
import Aux from '../../../../../hoc/Auxiliary'

import userImg from '../../../../../assets/useravatar.png'
import classes from './ReviewHeader.module.css';

const reviewHeader = (props) => {
    let header = null;
    if (props.show) {
        header = (
            <div className={classes.ReviewHeader}>
                <User image={userImg} userName={props.username} />  
                <p style={{fontSize: "1.1rem"}}>{props.date.toLocaleDateString()}</p>
            </div>
        )
    }
    return (
        <Aux>
            {header}
        </Aux>
        
    );
}

export default reviewHeader;