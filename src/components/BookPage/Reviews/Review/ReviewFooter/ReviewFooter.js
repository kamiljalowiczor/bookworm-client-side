import React, { Component } from 'react';

import Button from '../../../../UI/Button/Button'
import Aux from '../../../../../hoc/Auxiliary'

import classes from './ReviewFooter.module.css'
// eslint-disable-next-line
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus, faMinus } from "@fortawesome/free-solid-svg-icons";

class ReviewFooter extends Component {
    state = {
        upvotes: this.props.upvotes, //this.props.upvotes
        downvotes: this.props.downvotes //this.props.downvotes
        //rated: false
    }

    componentDidMount() {
        // tak na dobra sprawe to to chyba bedzie podobny plik do tego StarRating, chyba ze z backendu dostane od razu ratingi + - i id uzytkownika ktory napisal te recenzje. wtedy wystarczy bedzie sprawdzic id tego uzytkownika zalogowanego i zmieniac/wysylac glosy
    }

    postRating(vote) {
        // TODO
        // if (uzytkownik zaglosowal) { nic nie rob jezeli glos jest taki sam / zmien glos (np upvote +1, downvote -1)}
    }

    onRatingButtonClicked(e, vote) {
        // trzeba najpierw sprawdzic czy user moze juz zaglosowal, pewnie pole w reduxie
        //jezeli uzytkownik nie jest zalogowany to nie moze oceniac essa
        vote > 0 ? this.setState({ upvotes: this.state.upvotes + 1 }) : this.setState({ downvotes: this.state.downvotes + 1 })
        this.postRating(vote); // jak nowy glos
        // this.putRating(vote) // jak zmiana
    }

    render() {
        let footer = null;
        if (this.props.show) {
            footer = (
                <div className={classes.ReviewFooter}>
                    <div className={classes.Ratings}>
                        <div style={{ width: "130px", height: "50px", margin: "0" }}></div>
                        {/* <Button
                            style={{ width: "130px", margin: "0" }} btnType="Success"
                            clicked={(e) => this.onRatingButtonClicked(e, 1)}>
                            <FontAwesomeIcon icon={faPlus} style={{ color: "#6ccf33", filter: "drop-shadow(0 0 0.1rem black)" }} /> {this.state.upvotes}
                        </Button> */}
                        {/* <Button
                            style={{ width: "130px", margin: "0" }} btnType="Danger"
                            clicked={(e) => this.onRatingButtonClicked(e, -1)}>
                            <FontAwesomeIcon icon={faMinus} style={{ color: "rgb(218, 64, 64)", filter: "drop-shadow(0 0 0.1rem black)" }} /> {this.state.downvotes}
                        </Button> */}
                    </div>
                </div>
            )
        }

        return (
            <Aux>
                {footer}
            </Aux>
        )
    }
}

export default ReviewFooter;