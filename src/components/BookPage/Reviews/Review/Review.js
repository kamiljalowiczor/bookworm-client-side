import React from 'react';

import ReviewHeader from './ReviewHeader/ReviewHeader'
import ReviewFooter from './ReviewFooter/ReviewFooter'
import StarRating from '../../../UI/StarRating/StarRating'

import classes from './Review.module.css';

const review = (props) => {
    return (
        <div className={classes.ReviewCard}>
           <ReviewHeader 
                show={!props.noHeader} 
                username={props.username}
                date={props.date}/>
            <div className={classes.ReviewContent}>
                <p>{props.review}</p>
                <div className={classes.Rating}>
                    <StarRating dontShow={props.noRating} bookId={props.bookId} userId={props.username} size="2x" readonly />
                </div>     
            </div>
            <ReviewFooter 
                upvotes={props.upvotes}
                downvotes={props.downvotes}
                show={!props.noFooter}/> 
        </div>
    )
}

export default review;