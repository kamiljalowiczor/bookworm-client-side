import React from 'react';

import Heading from '../../UI/Text/Heading/Heading';
import Image from '../../UI/Image/Image';

import classes from './BookResult.module.css';

const bookResult = props => {
    return (
        <div className={classes.BookResult} onClick={props.onClick}>
            <div className={classes.BookImage}>
                <Image size="medium" src={props.image} alt="topbook" />
            </div>
            <div className={classes.BookInfo}>
                <Heading size="big">{props.name}</Heading>
                <ul>
                    <li><h5>{props.descr}</h5></li>
                    <li>Rating: {props.rating}/5.00</li>
                    <li>Released: {props.released}</li>
                </ul>
            </div>
        </div>
    );
}

export default bookResult;
