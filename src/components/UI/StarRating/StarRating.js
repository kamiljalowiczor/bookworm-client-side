import React, { Component } from 'react';
import Rating from 'react-rating';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from "@fortawesome/free-solid-svg-icons";
import axios from 'axios';
import instanceAxios from '../../../axios-requests.js';

import Aux from '../../../hoc/Auxiliary'
import Button from '../Button/Button';
import classes from './StarRating.module.css';

class StarRating extends Component {
    state = {
        rating: this.props.rating ? this.props.rating : null,
        rated: false,
        key: null,
        isMounted: false
    }

    componentDidMount() {
        console.log(this.props.userId);
        this.setState({ isMounted: true });
        if (this.state.rating === null) {
            if (this.props.userId !== null && this.props.userId !== undefined) {
                const queryParams = `?orderBy="userId"&equalTo="${this.props.userId}"`;
                axios.get(`https://bookworm-1bd60.firebaseio.com/ratings/${this.props.bookId}.json` + queryParams)
                    .then(res => {
                        const data = [];
                        for (let key in res.data) {
                            data.push({
                                ...res.data[key],
                                id: key
                            });
                        }
                        if (this.state.isMounted) {
                            this.setState({ rating: data[0].rating, rated: true, key: data[0].id })
                        }
                    })
                    .catch(err => {
                        console.log(err);
                    })
            }
        }
    }

    componentWillUnmount() {
        this.setState({ isMounted: false });
    }

    postRating = (ratingData) => {
        //na firebase
        axios.post(`https://bookworm-1bd60.firebaseio.com/ratings/${this.props.bookId}.json`, ratingData)
            .then(res => {
                this.setState({ rating: ratingData.rating, rated: true, key: res.data.name })
            })
            .catch(err => {
               // console.log(err);
            })

        //na backend
        console.log(ratingData.rating);
        instanceAxios.post(`books/addBookRate/${this.props.bookId}`, ratingData.rating, { headers: { 'Content-Type': 'application/json' } })
            .then(res => {
               // console.log(res.data);
            })
            .catch(err => {
                //console.log(err);
            })
    }

    putRating = (ratingData) => {
        axios.put(`https://bookworm-1bd60.firebaseio.com/ratings/${this.props.bookId}/${this.state.key}.json`, ratingData)
            .then(res => {
               // console.log(res);
            })
            .catch(err => {
                //console.log(err);
            })
    }

    deleteRating = () => {
        axios.delete(`https://bookworm-1bd60.firebaseio.com/ratings/${this.props.bookId}/${this.state.key}.json`)
            .then(res => {
                //console.log(res);
                this.setState({ rating: 0, rated: false });
            })
            .catch(err => {
                //console.log(err);
            })
    }

    onStarClick = (e) => {
        const ratingData = {
            userId: this.props.userId,
            rating: e
        }
        if (this.state.rated) {
            this.putRating(ratingData)
        }
        else {
            this.postRating(ratingData)
        }
    }

    onResetClick = () => {
        if (this.state.rated) {
            const ratingData = {
                userId: this.props.userId,
                rating: 0,
            }
            this.deleteRating(ratingData);
        }

    }

    render() {
        let bottomText = null
        if (!this.props.readonly) {
            bottomText = (<p style={{ color: 'lime' }}>Log in to rate</p>)
        }

        let starRating = (<div className={classes.Rating}>
            <Rating
                initialRating={this.state.rating}
                readonly={this.props.readonly || !this.props.userId}
                stop={5}
                emptySymbol={<FontAwesomeIcon size={this.props.size} icon={faStar} style={{ color: "#222" }} />}
                fullSymbol={<FontAwesomeIcon size={this.props.size} icon={faStar} style={{ color: "#ffc011" }} />}
                fractions={2}
                onClick={(e) => this.onStarClick(e)} />
            {this.props.userId && this.props.canBeReset
                ? <Button clicked={() => this.onResetClick()} btnType="Danger">Reset</Button>
                : bottomText}
        </div>);

        if (this.props.dontShow) {
            starRating = null;
        }

        return (
            <Aux>
                {starRating}
            </Aux>
        )
    }
}


export default StarRating;