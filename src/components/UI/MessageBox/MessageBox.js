import React from 'react' 

import Heading from '../Text/Heading/Heading'
import Input from '../Input/Input'
import Button from '../../UI/Button/Button'
import classes from './MessageBox.module.css'

const messageBox = (props) => {
    return (
        <div className={classes.MessageBox}>
            <Heading size="small" style={{margin: "0"}} sectionTitle>{props.title}</Heading>
            <div className={classes.Inside}>
                {props.children}             
                <Input elementType="textarea" 
                value={props.value}
                changed={props.changed}
                styleConfig={props.inputConfig}/>
            </div>
            <Heading size="small" style={{padding: "0", margin: "0"}} sectionTitle>
                <Button clicked={props.btnClicked} style={{width:"130px", margin:"0"}} btnType="Success">Submit</Button>
            </Heading>
        </div>
    );
}

export default messageBox;