import React from 'react';
import classes from './Heading.module.css';

const heading = (props) => {
    let classList = [];
    if (props.size === "large") {
        classList.push(classes.large);
    } 
    else if (props.size === "big") {
        classList.push(classes.big);
    }
    else if (props.size === "medium") {
        classList.push(classes.medium);
    }
    else {
        classList.push(classes.small);
    }

    if (props.sectionTitle) {
        classList.push(classes.sectionTitle)
    }

    return (
        <h2 className={classList.join(' ')} style={props.style}>{props.children}</h2>
    )
}

export default heading;

