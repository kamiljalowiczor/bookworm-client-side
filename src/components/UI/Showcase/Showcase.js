import React from 'react'

import Aux from '../../../hoc/Auxiliary'
import styled from 'styled-components'
import classes from './Showcase.module.css';

const showcase = (props) => {
    let height = null;
    let style = null;
    switch (props.size) {
        case 'large':
            height = 100;
            style = classes.LargeShowcaseContainer;
            break;
        case 'small':
            height = 40;
            style = classes.SmallShowcaseContainer;
            break;
        case 'normal':
        default:
            height = 60;
            style = classes.ShowcaseContainer;
            break;
    }

    // nie ruszac tego blagam was - nie przenosic nigdzie, nie dotykac, najlepiej nawet nie patrzec
    const StyledDiv = styled.div`
        width: 100%;
        height: ${height}vh;
        background: rgba(0, 0, 0, 0.6);
        display: block;
        position: relative;
        color: white;
        &::before { 
            content: '';
            background: url('${props.backgroundImage}')
              no-repeat center center/cover;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: -1;
            opacity: 1;
        }

        @media (max-width: 500px) {
            height: calc(100vh - 56px);
        }

        @media (max-height: 500px) {
            height: calc(180vh - 56px);
        }
    `

    return (
        <Aux>
            <StyledDiv>
                <div className={classes.Container}>
                    <div className={style}>
                        {props.left}
                        {props.right}
                    </div>
                </div>
            </StyledDiv>
        </Aux>
    );
}

export default showcase;