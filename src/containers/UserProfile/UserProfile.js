import React, { Component } from 'react';
import { connect } from 'react-redux';

import Showcase from '../../components/UI/Showcase/Showcase';
import Section from '../../components/UI/Section/Section';
import List from '../../components/UI/List/List';
import Spinner from '../../components/UI/Spinner/Spinner';
import TopBook from '../../components/AuthorPage/TopBooks/TopBook/TopBook';
import UserInfo from '../../components/UserInfo/UserInfo';

import image from '../../assets/bookworkm-homepage-showcase-pic.jpg';
import classes from '../../components/AuthorPage/Showcase/AuthorInfo/AuthorInfo.module.css';
import * as actions from '../../store/actions/';

class UserProfile extends Component {
    componentDidMount() {
        this.props.onBooksFetch(this.props.match.params.user);
        this.props.onShowcaseFetch(this.props.match.params.user);
    }

    prepareBook = book => {
        return (
            <li key={book.id}>
                <TopBook book={book} size="medium"/>
            </li>
        );
    }

    render() {
        let bookList = null;
        if (!this.props.loadingBooks && this.props.books && this.props.books.length > 0) {
           // bookList = (
                // <Section title="Favourite books">
                //     <List>
                //         {this.props.books.map(book => this.prepareBook(book))}
                //     </List>
                // </Section>
           // );
        } else if (!this.props.loadingBooks) {
         //   bookList = (
                // <Section title="Favourite books">
                //     <List>
                //         <li key="essa"><p style={{color: "#222", padding: '1rem'}}>This user hasn't rated any books yet.</p></li>
                //     </List>
                // </Section>
          //  );
        }

        let userInfo = <Spinner />;
        if (!this.props.loadingShowcase && this.props.showcase) {
            userInfo = (
                <UserInfo username={this.props.showcase.username} fullname={this.props.showcase.userFullname} email={this.props.showcase.email} />
            );
        } else if (!this.props.loadingShowcase) {
            userInfo = (
                <UserInfo username="failed to load" fullname="failed to load" email="failed to load" />
            );
        }

        let page = (
            <Showcase
                size="large"
                backgroundImage={image}
                left={userInfo}
                right={<div className={classes.InfoContainer}>
                    <div></div>
                    <div style={{textAlign: 'center'}}>
                        {bookList}
                    </div>
                </div>} />
        );

        return (
            <div>
                {page}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loadingBooks: state.user.loadingBooks,
        books: state.user.books,
        errorBooks: state.user.errorBooks,
        showcase: state.user.showcase,
        loadingShowcase: state.user.loadingShowcase
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onBooksFetch: username => dispatch(actions.booksFetch(username)),
        onShowcaseFetch: username => dispatch(actions.showcaseFetch(username))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
