import React, { Component } from 'react';
import { connect } from 'react-redux';

import Spinner from '../../components/UI/Spinner/Spinner';
import Section from '../../components/UI/Section/Section';
import List from '../../components/UI/List/List';
import AuthorResult from '../../components/SearchResults/AuthorResult/AuthorResult';
import BookResult from '../../components/SearchResults/BookResult/BookResult';
import UserResult from '../../components/SearchResults/UserResult/UserResult';

import authorImage from '../../assets/authoravatar.png';
import userImage from '../../assets/useravatar.png';

import * as actions from '../../store/actions/';

class SearchPage extends Component {
    state = {
        searchType: 'bookName',
        searchText: ''
    }

    componentDidMount() {
        this.loadQuery();
    }

    componentDidUpdate() {
        this.loadQuery();
    }

    loadQuery() {
        const query = new URLSearchParams(this.props.location.search);
        for (let param of query.entries()) {
            if (!this.state.searchType || this.state.searchType !== param[0] ||
                !this.state.searchText || this.state.searchText !== param[1]) {
                this.setState({
                    searchType: param[0],
                    searchText: param[1]
                });
                this.props.onSearch(param[0], param[1]);
            }
        }
    }

    prepareResult = (category, result) => {
        switch (category) {
            case 'bookName':
                if (result.bookAverageRate === null) {
                    result.bookAverageRate = "0"
                }

                let description = result.description; 
                if (description === "No description") {
                    description = "No description available for this book.";
                }

                return (
                    <li key={result.id}>
                        <BookResult
                            image={result.thumbnailUrl}
                            name={result.title}
                            descr={description}
                            rating={parseFloat(result.bookAverageRate)}
                            released={result.publishedDate}
                            onClick={event => this.onResultClickHandler(event, category, result.id)} />
                    </li>
                );
            case 'authorName':
                return (
                    <li key={result.id}>
                        <AuthorResult
                            image={authorImage}
                            name={result.name}
                            birthplace={result.birthplace}
                            descr={result.description}
                            onClick={event => this.onResultClickHandler(event, category, result.name)} />
                    </li>
                );
            case 'userName':
                return (
                    <li key={result.id}>
                        <UserResult
                            image={userImage}
                            fullname={result.userFullname}
                            username={result.username}
                            descr={result.userDescription}
                            onClick={event => this.onResultClickHandler(event, category, result.username)} />
                    </li>
                );
            default:
                return (
                    <li>
                        <p>UNKNOWN CATEGORY</p>
                    </li>
                );
        }
    }

    onResultClickHandler = (event, category, id) => {
        event.preventDefault();
        if (category === 'bookName') {
            this.props.history.push('/book/' + id);
        } else if (category === 'authorName') {
            this.props.history.push('/author/' + id);
        } else {
            this.props.history.push('/user/' + id);
        }
    }

    render() {
        let results = <Spinner />;
        if (!this.props.loading && this.props.results && this.props.results.length > 0) {
            results = this.props.results.map(result => this.prepareResult(this.props.category, result));
        } else if (!this.props.loading) {
            results = <li key="essa"><p>Your search did not match any documents.</p></li>;
        }

        return (
            <div>
                <Section title="Results">
                    <List>
                        {results}
                    </List>
                </Section >
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.search.loading,
        category: state.search.category,
        results: state.search.results
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSearch: (searchType, searchText) => dispatch(actions.search(searchType, searchText))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
