import React, { Component} from 'react'
import { connect } from 'react-redux';

import AuthorInfo from '../../components/AuthorPage/Showcase/AuthorInfo/AuthorInfo'
import AuthorBio from '../../components/AuthorPage/Showcase/AuthorBio/AuthorBio'
import Showcase from '../../components/UI/Showcase/Showcase'
import LatestReview from '../../components/AuthorPage/LatestReview/LatestReview'
import Aux from '../../hoc/Auxiliary'
import Spinner from '../../components/UI/Spinner/Spinner'

import AllBooks from '../../components/AuthorPage/AllBooks/AllBooks'
import TopBooks from '../../components/AuthorPage/TopBooks/TopBooks'
import image from '../../assets/low-light-photography-of-books-1301585.jpg'
import classes from './AuthorPage.module.css';

import { deepCopy } from '../../store/utility'

import * as actions from '../../store/actions/index'

class AuthorPage extends Component {

    componentDidMount() {
        const query = new URLSearchParams(this.props.match.params);
        let authorName = query.get('author');

        if (authorName) {    
            this.props.onFetchAuthor(authorName);
        }
    }

    sortBooks(books, key) {
        switch (key) {
            case "publishedDate":
                books.sort((a, b) => (
                    new Date(b[key]).getTime() -  new Date(a[key]).getTime()));
                return books;
            case "bookAverageRate": 
                books.sort((a, b) => (
                    b[key] - a[key]
                ));
                return books;
            default: 
                return books;
        }
    }

    calculateAverageBookRating() {
        let ratedBooks = 0;
        let average = 0;
        this.props.allBooks.forEach(book => {
            if (book.bookAverageRate) {
                average = average + book.bookAverageRate
                ratedBooks++;
            }
        })
        return ratedBooks === 0 ? 0 : average / ratedBooks;
    }

    render() {
        let page = <Spinner />;
        if (!this.props.loading && this.props.author && this.props.allBooks) {
            const author = {
                name: this.props.author.name,
                description: this.props.author.description,
                birthplace: this.props.author.birthplace
            };

            const topBooks = this.sortBooks(deepCopy(this.props.allBooks), "bookAverageRate");
            const allBooks = this.sortBooks(deepCopy(this.props.allBooks), "publishedDate")
            console.log(allBooks);

            let latestReview = null;
            if (this.props.latestReview) {
                latestReview = (
                    <LatestReview 
                        userName={"|| MISSING ||"} 
                        review={this.props.latestReview}
                     /> 
                )
            }

            page = (    
                <Aux>
                    <Showcase
                        size="normal"
                        backgroundImage={image}
                        left={<AuthorInfo 
                            name={author.name} />}
                        right={<AuthorBio 
                            booksReleasedAmount={this.props.booksReleasedAmount}
                            averageRating={this.calculateAverageBookRating()} />}>
                    </Showcase>
                    <div className={classes.Page}>
                        <div className={classes.Left}>
                            {latestReview}
                            <AllBooks 
                                author={author.name} 
                                books={allBooks} />            
                        </div>
                        <div className={classes.Right} >
                            <TopBooks books={topBooks} />   
                        </div>            
                    </div>
                 </Aux>
            )
        }
        return (
            <Aux>
                {page}
            </Aux>
        );
        
    }
}

const mapStateToProps = state => {
    return {
        loading: state.author.loadingAuthor || state.author.loadingLatestReview || state.author.loadingAllBooks,
        author: state.author.author,
        allBooks: state.author.allBooks,
        booksReleasedAmount: state.author.booksReleasedAmount,
        latestReview: state.author.latestReview
    }
}

const mapDispatchToProps = (dispatch, state) => {
    return {
        onFetchAuthor: (authorName) => dispatch(actions.fetchAuthor(authorName))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthorPage);