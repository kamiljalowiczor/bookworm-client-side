import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import Button from '../../../components/UI/Button/Button';
import Input from '../../../components/UI/Input/Input';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Heading from '../../../components/UI/Text/Heading/Heading';
import classes from './SignUp.module.css';
import * as actions from '../../../store/actions/';

class SignUp extends Component {
    state = {
        controls: {
            username: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter your username'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            },
            userFullname: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter your full name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Enter your e-mail'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Enter your password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            },
            confirmPassword: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Confirm your password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6,
                    identicalPassword: true
                },
                valid: false,
                touched: false
            }
        }
    };

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid;
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid;
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid;
        }

        if (rules.identicalPassword) {
            isValid = value === this.state.controls.password.value && isValid;
        }

        return isValid;
    }

    isFormValid() {
        for (let key in this.state.controls) {
            if (!this.state.controls[key].valid) {
                return false;
            }
        }
        return true;
    }

    submittedOnEmptyForm = () => {
        let updatedControls = { ...this.state.controls };
        for (let key in this.state.controls) {
            updatedControls = {
                ...updatedControls,
                [key]: {
                    ...this.state.controls[key],
                    touched: true
                }
            };
        }
        this.setState({ controls: updatedControls });
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            }
        };
        this.setState({ controls: updatedControls });
    }

    submitHandler = event => {
        event.preventDefault();
        if (this.props.registered) {
            this.props.history.push('/log-in');
        } else if (this.isFormValid()) {
            this.props.onSignUp(
                this.state.controls.username.value,
                this.state.controls.userFullname.value,
                this.state.controls.email.value,
                this.state.controls.password.value
            );
        } else {
            this.submittedOnEmptyForm();
        }
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key],
            });
        }

        let form = formElementsArray.map(formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));

        if (this.props.loading) {
            form = <Spinner />;
        }

        if (this.props.registered) {
            form = (
                <div>
                    <Heading size="big">Great!</Heading>
                    <p>Your account has been successfully created!</p>
                    <p>We have sent you an email with a confirmation link to your email address.</p> 
                    <p>In order to complete the sign-up process, please go to your email box and click the confirmation link.</p>
                    <p>Then, come back here, click the button below and log in to your freshly created account!</p>
                </div>
            );
        }

        let errorMessage = null;

        if (this.props.responseCode === 409 || this.props.responseCode === 'Request failed with status code 409') {
            errorMessage = (
                <p style={{ color: 'red' }}>User with this username or email address already exists</p>
            );
        } else if (this.props.responseCode !== null && this.props.responseCode !== 200) {
            errorMessage = (
                <p style={{ color: 'red' }}>Unknown error</p>
            );
        }

        return (
            <div className={classes.SignUp}>
                {errorMessage}
                <form onSubmit={this.submitHandler}>
                    {form}
                    <Button btnType="Success">{this.props.registered ? 'I have confirmed my account!' : 'Sign up!'}</Button>
                </form>
                Already have an account? <NavLink to="/log-in">Log in here</NavLink>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        responseCode: state.auth.responseCode,
        registered: state.auth.registered
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSignUp: (username, userFullname, email, password) => dispatch(actions.signUp(username, userFullname, email, password))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
