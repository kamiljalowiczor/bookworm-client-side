import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

import Button from '../../../components/UI/Button/Button';
import Input from '../../../components/UI/Input/Input';
import Spinner from '../../../components/UI/Spinner/Spinner';
import classes from './ResetPassword.module.css';
import * as actions from '../../../store/actions/';

class ResetPassword extends Component {
    state = {
        controls: {
            username: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter your username'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            }
        }
    };

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        return isValid;
    }

    isFormValid() {
        for (let key in this.state.controls) {
            if (!this.state.controls[key].valid) {
                return false;
            }
        }
        return true;
    }

    submittedOnEmptyForm = () => {
        let updatedControls = { ...this.state.controls };
        for (let key in this.state.controls) {
            updatedControls = {
                ...updatedControls,
                [key]: {
                    ...this.state.controls[key],
                    touched: true
                }
            };
        }
        this.setState({ controls: updatedControls });
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            }
        };
        this.setState({ controls: updatedControls });
    }

    submitHandler = event => {
        event.preventDefault();
        if (this.props.reset) {
            this.props.history.push('/log-in');
        } else if (this.isFormValid()) {
            this.props.onResetPassword(this.state.controls.username.value);
        } else {
            this.submittedOnEmptyForm();
        }
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key],
            });
        }

        let form = formElementsArray.map(formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));

        if (this.props.loading) {
            form = <Spinner />;
        }

        const infoMessage = (
            <p>Enter your username and we will send a new password to your email address.</p>
        );

        if (this.props.reset) {
            form = (
                <div>
                    <p>We have sent you an email with your new password.</p>
                    <p>Click the button below and log in with the password you have received.</p>
                </div>
            );
        }

        let errorMessage = null;

        if (this.props.responseCode === 409 || this.props.responseCode === 'Request failed with status code 409') {
            errorMessage = (
                <p style={{ color: 'red' }}>User with this username does not exist.</p>
            );
        } else if (this.props.responseCode !== null && this.props.responseCode !== 200) {
            errorMessage = (
                <p style={{ color: 'red' }}>Unknown error</p>
            );
        }

        return (
            <div className={classes.ResetPassword}>
                {this.props.reset ? null : infoMessage}
                {errorMessage}
                <form onSubmit={this.submitHandler}>
                    {form}
                    <Button btnType="Success">{this.props.reset ? 'Take me to login' : 'Send new password'}</Button>
                </form>
                {this.props.reset ? null : <p><NavLink to="/log-in">Back to login</NavLink></p>}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        responseCode: state.auth.responseCode,
        reset: state.auth.reset
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onResetPassword: (username) => dispatch(actions.resetPassword(username))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);
