import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import Button from '../../../components/UI/Button/Button';
import Input from '../../../components/UI/Input/Input';
import Spinner from '../../../components/UI/Spinner/Spinner';
import classes from './LogIn.module.css';
import * as actions from '../../../store/actions/';

class LogIn extends Component {
    state = {
        controls: {
            username: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter your username'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Enter your password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            }
        }
    };

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    isFormValid() {
        for (let key in this.state.controls) {
            if (!this.state.controls[key].valid) {
                return false;
            }
        }
        return true;
    }

    submittedOnEmptyForm = () => {
        let updatedControls = {...this.state.controls};
        for (let key in this.state.controls) {
            updatedControls = {
                ...updatedControls,
                [key]: {
                    ...this.state.controls[key],
                    touched: true
                }
            };
        }
        this.setState({ controls: updatedControls });
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            }
        };
        this.setState({ controls: updatedControls });
    }

    submitHandler = event => {
        event.preventDefault();
        if (this.isFormValid()) {
            this.props.onLogIn(this.state.controls.username.value, this.state.controls.password.value);
        } else {
            this.submittedOnEmptyForm();
        }
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key],
            });
        }

        let form = formElementsArray.map(formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));

        if (this.props.loading) {
            form = <Spinner />;
        }

        let errorMessage = null;

        if (this.props.responseCode === 401 || this.props.responseCode === 'Request failed with status code 401') {
            errorMessage = (
                <p style={{color:'red'}}>Invalid username or password</p>
            );
        } else if (this.props.responseCode !== null && this.props.responseCode !== 200) {
            errorMessage = (
                <p style={{color:'red'}}>Unknown error</p>
            );
        }

        return (
            <div className={classes.LogIn}>
                {errorMessage}
                <form onSubmit={this.submitHandler}>
                    {form}
                    <Button btnType="Success">Log in!</Button>
                </form>
                <p><NavLink to="/reset-password">Forgot password?</NavLink></p>
                {this.props.registered ? null : <p>Don't have an account? <NavLink to="/sign-up">Sign up here</NavLink></p>}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        responseCode: state.auth.responseCode,
        registered: state.auth.registered
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onLogIn: (username, password) => dispatch(actions.logIn(username, password))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LogIn);
