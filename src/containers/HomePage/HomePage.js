import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

import Showcase from '../../components/UI/Showcase/Showcase';
import Heading from '../../components/UI/Text/Heading/Heading';
import Button from '../../components/UI/Button/Button';
import Section from '../../components/UI/Section/Section';
import List from '../../components/UI/List/List';
import Spinner from '../../components/UI/Spinner/Spinner';
import Image from '../../components/UI/Image/Image';
import TopBook from '../../components/AuthorPage/TopBooks/TopBook/TopBook';

import userImage from '../../assets/useravatar.png';
import image from '../../assets/bookworkm-homepage-showcase-pic.jpg';
import classes from '../../components/AuthorPage/Showcase/AuthorInfo/AuthorInfo.module.css';
import * as actions from '../../store/actions/';

class HomePage extends Component {
    componentDidMount() {
        if (this.props.username) {
            this.props.onBooksFetch(this.props.username);
        }
    }

    loginClickedHandler = event => {
        event.preventDefault();
        this.props.history.push('/sign-up');
    }

    prepareBook = book => {
        return (
            <li key={book.id}>
                <TopBook book={book} size="medium"/>
            </li>
        );
    }

    render() {
        let bookList = null;
        if (!this.props.loadingBooks && this.props.books && this.props.books.length > 0) {
            // bookList = (
            //     <Section title="Your favourite books">
            //         <List>
            //             {this.props.books.map(book => this.prepareBook(book))}
            //         </List>
            //     </Section>
            // );
        } else if (!this.props.loadingBooks) {
            // bookList = (
            //     <Section title="Your favourite books">
            //         <List>
            //             <li key="essa"><p style={{color: "#222", padding: '1rem'}}>You haven't rated any books yet!</p></li>
            //         </List>
            //     </Section>
            // );
        }

        let homepage = (
            <Showcase
                size="large"
                backgroundImage={image}
                left={<div className={classes.InfoContainer}>
                    <div></div>
                    <div style={{textAlign: 'center'}}>
                        <Heading size="large">Welcome to Bookworm!</Heading>
                        <p>Bookworm is an amazing service that allows users to share their opinions about their favourite books!</p>
                    </div>
                </div>}
                right={<div className={classes.InfoContainer}>
                    <div></div>
                    <div style={{textAlign: 'center'}}>
                        <Heading size="medium">Join us now!</Heading>
                        <div>
                            <Button
                                style={{backgroundColor: '#ffc011'}}
                                clicked={this.loginClickedHandler}>Sign up for free!</Button>
                            <div>
                                Already have an account? <NavLink to="/log-in" style={{color: '#ffc011'}}>Log in here!</NavLink>
                            </div>
                        </div>
                    </div>
                </div>} />
        );

        if (this.props.isAuthenticated) {
            homepage = (
                <Showcase
                    size="large"
                    backgroundImage={image}
                    left={<div className={classes.InfoContainer}>
                            <Image round size="big" src={userImage} alt="user" />
                            <Heading size="big">Hello {this.props.username}</Heading>
                    </div>}
                    right={<div className={classes.InfoContainer}>
                        <div style={{textAlign: 'center'}}>
                            {bookList}
                        </div>
                    </div>} />
            );
        }

        return (
            <div className={classes.NegativeMargin}>
                {homepage}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        username: state.auth.userId,
        loadingBooks: state.user.loadingBooks,
        books: state.user.books,
        errorBooks: state.user.errorBooks
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onBooksFetch: username => dispatch(actions.booksFetch(username))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
