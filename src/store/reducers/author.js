import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loadingAuthor: true,
    loadingLatestReview: true,
    loadingAllBooks: true,
    author: null,
    latestReview: null,
    allBooks: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case (actionTypes.AUTHOR_FETCH_START): 
            return {
                ...state, 
                loadingAuthor: true
            };
        case (actionTypes.AUTHOR_FETCH_FAIL): 
            return {
                ...state,
                loadingAuthor: false
            }
        case (actionTypes.AUTHOR_FETCH_SUCCESS): 
            console.log(action.author);
            return {
                ...state,
                author: {
                    name: action.author.name,
                    description: action.author.description,
                    birthplace: action.author.birthplace
                },
                loadingAuthor: false
            }       
        case (actionTypes.AUTHOR_LATEST_REVIEW_FETCH_START): 
            return {
                ...state, 
                loadingLatestReview: true
            }
        case (actionTypes.AUTHOR_LATEST_REVIEW_FETCH_FAIL): 
            return {
                ...state, 
                loadingLatestReview: false
            }  
        case (actionTypes.AUTHOR_LATEST_REVIEW_FETCH_SUCCESS): 
            console.log(action.review);
            return {
                ...state, 
                latestReview: action.review,
                loadingLatestReview: false
            }
        case (actionTypes.AUTHOR_ALLBOOKS_FETCH_START): 
            return {
                ...state,
                loadingAllBooks: true
            }
        case (actionTypes.AUTHOR_ALLBOOKS_FETCH_FAIL): 
            return {
                ...state,
                loadingAllBooks: false
            }
        case (actionTypes.AUTHOR_ALLBOOKS_FETCH_SUCCESS): 
          //  console.log(action.allBooks);
            return {
                ...state,
                allBooks: action.allBooks,
                booksReleasedAmount: action.allBooks.length,
                loadingAllBooks: false
            }
        default: 
            return state;       
    }
}

export default reducer;