import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    isAuthenticated: false,
    userId: null,
    responseCode: null,
    loading: false,
    registered: null,
    reset: null
};

const authStart = (state, action) => {
    return updateObject(state, {responseCode: null, loading: true});
}

const authSuccess = (state, action) => {
    return updateObject(state, {
        userId: action.username,
        isAuthenticated: action.authState,
        responseCode: action.responseCode,
        loading: false,
        registered: null,
        reset: null
    });
}

const authFail = (state, action) => {
    return updateObject(state, {
        responseCode: action.responseCode,
        loading: false
    });
}

const authLogOut = (state, action) => {
    return updateObject(state, {
        isAuthenticated: false,
        responseCode: null,
        userId: null,
        registered: null
    });
}

const authRegister = (state, action) => {
    return updateObject(state, {
        registered: action.registered,
        responseCode: action.responseCode,
        loading: false
    });
}

const authResetPassword = (state, action) => {
    return updateObject(state, {
        reset: action.reset,
        responseCode: action.responseCode,
        loading: false
    });
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.RESET_PASSWORD_START:
        case actionTypes.REGISTER_START:
        case actionTypes.AUTH_START:
            return authStart(state, action);
        case actionTypes.AUTH_SUCCESS:
            return authSuccess(state, action);
        case actionTypes.AUTH_FAIL:
            return authFail(state, action);
        case actionTypes.REGISTER_SUCCESS:
        case actionTypes.REGISTER_FAIL:
            return authRegister(state, action);
        case actionTypes.RESET_PASSWORD_SUCCESS:
        case actionTypes.RESET_PASSWORD_FAIL:
            return authResetPassword(state, action);
        case actionTypes.AUTH_LOGOUT_SUCCESS:
            return authLogOut(state, action);
        case actionTypes.AUTH_LOGOUT_START:
        case actionTypes.AUTH_LOGOUT_FAIL:
        default:
            return state;
    }
}

export default reducer;
