import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    loadingBooks: false,
    loadingRatings: false,
    loadingShowcase: false,
    books: [],
    ratings: [],
    showcase: null,
    errorBooks: null,
    errorRatings: null,
    errorShowcase: null
}

const booksFetchStart = (state, action) => {
    return updateObject(state, {loadingBooks: true});
}

const ratingsFetchStart = (state, action) => {
    return updateObject(state, {loadingRatings: true});
}

const showcaseFetchStart = (state, action) => {
    return updateObject(state, {loadingShowcase: true});
}

const booksFetchSuccess = (state, action) => {
    return updateObject(state, {loadingBooks: false, books: action.books});
}

const ratingsFetchSuccess = (state, action) => {
    return updateObject(state, {loadingRatings: false, ratings: action.ratings});
}

const showcaseFetchSuccess = (state, action) => {
    return updateObject(state, {loadingShowcase: false, showcase: action.showcase});
}

const booksFetchFail = (state, action) => {
    return updateObject(state, {loadingBooks: false, books: [], error: action.error});
}

const ratingsFetchFail = (state, action) => {
    return updateObject(state, {loadingRatings: false, ratings: [], error: action.error});
}

const showcaseFetchFail = (state, action) => {
    return updateObject(state, {loadingShowcase: false, showcase: null, error: action.error});
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.USER_BOOKS_FETCH_START:
            return booksFetchStart(state, action);
        case actionTypes.USER_BOOKS_FETCH_SUCCESS:
            return booksFetchSuccess(state, action);
        case actionTypes.USER_BOOKS_FETCH_FAIL:
            return booksFetchFail(state, action);
        case actionTypes.USER_RATINGS_FETCH_START:
            return ratingsFetchStart(state, action);
        case actionTypes.USER_RATINGS_FETCH_SUCCESS:
            return ratingsFetchSuccess(state, action);
        case actionTypes.USER_RATINGS_FETCH_FAIL:
            return ratingsFetchFail(state, action);
        case actionTypes.USER_SHOWCASE_FETCH_START:
            return showcaseFetchStart(state, action);
        case actionTypes.USER_SHOWCASE_FETCH_SUCCESS:
            return showcaseFetchSuccess(state, action);
        case actionTypes.USER_SHOWCASE_FETCH_FAIL:
            return showcaseFetchFail(state, action);
        default:
            return state;
    }
}

export default reducer;
