import axios from '../../axios-requests';
import * as actionTypes from './actionTypes';

axios.defaults.withCredentials = true;

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authSuccess = (authState, responseCode, username) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        authState: authState,
        responseCode: responseCode,
        username: username
    };
};

export const authFail = responseCode => {
    return {
        type: actionTypes.AUTH_FAIL,
        responseCode: responseCode
    };
};

export const registerStart = () => {
    return {
        type: actionTypes.REGISTER_START
    };
};

export const registerSuccess = responseCode => {
    return {
        type: actionTypes.REGISTER_SUCCESS,
        responseCode: responseCode,
        registered: true
    };
};

export const registerFail = responseCode => {
    return {
        type: actionTypes.REGISTER_FAIL,
        responseCode: responseCode,
        registered: false
    };
};

export const logOutStart = () => {
    return {
        type: actionTypes.AUTH_LOGOUT_START
    };
};

export const logOutSuccess = () => {
    return {
        type: actionTypes.AUTH_LOGOUT_SUCCESS
    };
};

export const logOutFail = () => {
    return {
        type: actionTypes.AUTH_LOGOUT_FAIL
    };
};

export const resetPasswordStart = () => {
    return {
        type: actionTypes.RESET_PASSWORD_START
    };
};

export const resetPasswordSuccess = responseCode => {
    return {
        type: actionTypes.RESET_PASSWORD_SUCCESS,
        responseCode: responseCode,
        reset: true
    };
};

export const resetPasswordFail = responseCode => {
    return {
        type: actionTypes.RESET_PASSWORD_FAIL,
        responseCode: responseCode,
        reset: false
    };
};

export const checkAuthTimeout = expirationTime => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logOut());
        }, expirationTime * 1000);
    }
};

export const logIn = (username, password) => {
    return dispatch => {
        dispatch(authStart());

        const authData = {
            id: 0,
            password: password,
            username: username
        };

        axios.post('/login', authData, { headers: { 'Content-Type': 'application/json' } })
            .then(response => {
                if (response.status === 200) {
                    const expiresIn = 3600;
                    const expirationDate = new Date(new Date().getTime() + expiresIn * 1000);
                    localStorage.setItem('expirationDate', expirationDate);
                    localStorage.setItem('userId', username);
                    dispatch(authSuccess(true, response.status, username));
                    dispatch(checkAuthTimeout(expiresIn));
                } else {
                    dispatch(authFail(response.status));
                }
            })
            .catch(err => {
                dispatch(authFail(err.message));
            });
    };
}

export const signUp = (username, userFullname, email, password) => {
    return dispatch => {
        dispatch(registerStart());

        const authData = {
            email: email,
            id: 0,
            password: password,
            userFullname: userFullname,
            username: username
        };

        axios.post('/register', authData, { headers: { 'Content-Type': 'application/json' } })
            .then(response => {
                if (response.status === 200) {
                    dispatch(registerSuccess(response.status));
                } else {
                    dispatch(registerFail(response.status));
                }
            })
            .catch(err => {
                dispatch(registerFail(err.message));
            });
    };
};

export const logOut = () => {
    return dispatch => {
        dispatch(logOutStart());
        axios.post('/logout')
            .then(response => {
                localStorage.removeItem('expirationDate');
                localStorage.removeItem('userId');
                dispatch(logOutSuccess());
            })
            .catch(err => {
                dispatch(logOutFail());
            });
    }
}

export const authCheckState = () => {
    return dispatch => {
        const userId = localStorage.getItem('userId');
        if (!userId) {
            dispatch(logOut());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate > new Date()) {
                dispatch(authSuccess(true, 200, userId));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
            } else {
                dispatch(logOut());
            }
        }
    }
};

export const resetPassword = username => {
    return dispatch => {
        dispatch(resetPasswordStart());
        axios.post('/reset?username=' + username)
            .then(response => {
                if (response.status === 200) {
                    dispatch(resetPasswordSuccess(response.status));
                } else {
                    dispatch(resetPasswordFail(response.status));
                }
            })
            .catch(err => {
                dispatch(resetPasswordFail(err.message));
            });
    }
};
