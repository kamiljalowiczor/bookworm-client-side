import axios from '../../axios-requests';
import * as actionTypes from './actionTypes';

export const fetchAuthorStart = () => {
    return {
        type: actionTypes.AUTHOR_FETCH_START,
    };
};

export const fetchAuthorFail = (error) => {
    return {
        type: actionTypes.AUTHOR_FETCH_FAIL,
        error: error
    };
}


export const fetchAuthorSuccess = (author) => {
    return {
        type: actionTypes.AUTHOR_FETCH_SUCCESS,
        author
    };
}

export const fetchLatestReviewStart = () => {
    return {
        type: actionTypes.AUTHOR_LATEST_REVIEW_FETCH_START,
    }
}

export const fetchLatestReviewFail = (error) => {
    return {
        type: actionTypes.AUTHOR_LATEST_REVIEW_FETCH_FAIL,
        error
    }
}

export const fetchLatestReviewSuccess = (review) => {
    return {
        type: actionTypes.AUTHOR_LATEST_REVIEW_FETCH_SUCCESS,
        review 
    }
}
    
export const fetchAllAuthorBooksStart = () => {
    return {
        type: actionTypes.AUTHOR_ALLBOOKS_FETCH_START,
    }
}

export const fetchAllAuthorBooksFail = (error) => {
    return {
        type: actionTypes.AUTHOR_ALLBOOKS_FETCH_FAIL,
        error
    }
}

export const fetchAllAuthorBooksSuccess = (allBooks) => {
    return {
        type: actionTypes.AUTHOR_ALLBOOKS_FETCH_SUCCESS,
        allBooks
    }
}

export const fetchAuthor = (authorName) => {
    return dispatch => {
        dispatch(fetchAuthorData(authorName));
        dispatch(fetchAllAuthorBooks(authorName));
        dispatch(fetchLatestReview(authorName));
    }
};

export const fetchAuthorData = (authorName) => {
    return dispatch => {
        dispatch(fetchAuthorStart());
        axios.get(`getAuthorInfo/${authorName}`)
        .then(res => {
            dispatch(fetchAuthorSuccess(res.data));   
        })
        .catch(err => {
            dispatch(fetchAuthorFail(err))
        })
    }
}

export const fetchLatestReview = (authorName) => {
    return dispatch => {
        dispatch(fetchLatestReviewStart());
        axios.get(`getLastBookReview/${authorName}`)
        .then(res=> {
            dispatch(fetchLatestReviewSuccess(res.data));
        })
        .catch(err => {
            console.log(err);
            dispatch(fetchLatestReviewFail(err))
        })
    }
}

export const fetchAllAuthorBooks = (authorName) => {
    return dispatch => {
        dispatch(fetchAllAuthorBooksStart());
        axios.get(`getAllAuthorBooks/${authorName}`)
        .then(res => {
            dispatch(fetchAllAuthorBooksSuccess(res.data));   
        })
        .catch(err => {
            dispatch(fetchAllAuthorBooksFail(err))
        })
    }
}