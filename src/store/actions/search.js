import axios from '../../axios-requests';
import * as actionTypes from './actionTypes';

export const searchStart = () => {
    return {
        type: actionTypes.SEARCH_START
    };
};

export const searchSuccess = (category, results) => {
    return {
        type: actionTypes.SEARCH_SUCCESS,
        category: category,
        results: results
    };
};

export const searchFail = error => {
    return {
        type: actionTypes.SEARCH_FAIL,
        error: error
    };
};

export const search = (category, query) => {
    return dispatch => {
        dispatch(searchStart());
        const queryParams = new URLSearchParams([[category, query]]);
        axios.get('/search', {params: queryParams})
            .then(response => {
                if (response.status === 200 && response.data) {
                    switch (response.data.category) {
                        case 'bookName':
                            dispatch(searchSuccess(response.data.category, response.data.foundBooks));
                            break;
                        case 'authorName':
                            dispatch(searchSuccess(response.data.category, response.data.foundAuthors));
                            break;
                        case 'userName':
                            dispatch(searchSuccess(response.data.category, response.data.foundUsers));
                            break;
                        default:
                            dispatch(searchSuccess(response.data.category, response.data.results));
                            break;
                    }
                } else {
                    dispatch(searchFail(response.status));
                }
            })
            .catch(error => {
                dispatch(searchFail(error));
            });
    };
};
